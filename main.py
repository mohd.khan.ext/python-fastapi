from fastapi import Depends, FastAPI, HTTPException
from sqlalchemy.orm import Session
from crud import get_user, get_user_by_email, get_users, create_user_api
from database import SessionLocal, engine, Base
import models
from schemas import User, UserCreate, UpdateUser

Base.metadata.create_all(bind=engine)

app = FastAPI()

# Dependency


def get_db():
    db = SessionLocal()
    try:
        yield db
    finally:
        db.close()

# Create user


@app.post("/users/", response_model=User)
def create_user(user: UserCreate, db: Session = Depends(get_db)):
    db_user = get_user_by_email(db, email=user.email)
    if db_user:
        raise HTTPException(status_code=400, detail="Email already registered")
    return create_user_api(db=db, user=user)

# Get all users


@app.get("/users/", response_model=list[User])
def read_users(skip: int = 0, limit: int = 100, db: Session = Depends(get_db)):
    users = get_users(db, skip=skip, limit=limit)
    return users
# Get specific user


@app.get("/users/{user_id}", response_model=User)
def read_user(user_id: int, db: Session = Depends(get_db)):
    db_user = get_user(db, user_id=user_id)
    if db_user is None:
        raise HTTPException(status_code=404, detail="User not found")
    return db_user
# Gets Update


@app.put("/users/{user_id}", response_model=UpdateUser)
def update_user(user_id: int, user: UpdateUser, db: Session = Depends(get_db)):
    db_user = get_user(db, user_id=user_id)
    print("This is my db user", db_user)
    if not db_user:
        raise HTTPException(status_code=404, detail="User not found")
    user_data = user.dict(exclude_unset=True)
    for key, value in user_data.items():
        setattr(db_user, key, value)
    db.add(db_user)
    db.commit()
    db.refresh(db_user)
    return db_user

# Delete User


@app.delete("/users/{user_id}")
def delete_user(user_id: int, db: Session = Depends(get_db)):
    # db_user = db.query(models.User).filter(models.User.id == user_id).first()
    db_user = get_user(db, user_id=user_id)
    if db_user is None:
        raise HTTPException(status_code=404, detail="User not found")
    db.delete(db_user)
    db.commit()
    return {"Ok": True}
