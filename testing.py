import requests

url = "http://127.0.0.1:8000/items/1234"
headers = {
        "content-type": "application/json",
        "server": "uvicorn"
    }

data = {
        "name": "Zaidkhan",
        "price": 12345,
        "is_offer": True
    }
r = requests.put(url,data)

r = r.json()
    
print(r)
